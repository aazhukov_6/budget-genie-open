from django.shortcuts import render, redirect
from .models import IncomeCategories, ExpenceCategories
from .forms import IncomeCategoriesCreate, ExpenceCategoriesCreate
from django.http import HttpResponse
# Create your views here.

#main page 
def index(request):
    income_shelf = IncomeCategories.objects.all()
    expence_shelf = ExpenceCategories.objects.all()
    return render(request, 'CRUD/CRUD.html', {'income_shelf': income_shelf, 'expence_shelf': expence_shelf})


#main_settings 
def settings(request):
    income_shelf = IncomeCategories.objects.all()
    expence_shelf = ExpenceCategories.objects.all()
    return render(request, 'CRUD/settings_main.html', {'income_shelf': income_shelf, 'expence_shelf': expence_shelf})

#CRUD for income categories
def upload_income(request):
    upload_income = IncomeCategoriesCreate()
    if request.method == 'POST':
        upload_income = IncomeCategoriesCreate(request.POST, request.FILES)
        if upload_income.is_valid():
            upload_income.save()
            return redirect('settings_main')
        else:
            return HttpResponse(""" Something went wrong. Please reload
              the webpage by clicking <a href = "{{url = 'settings_main'}}>Reload</a>"
              """)
    else:
        return render(request, 'CRUD/upload_income_form.html', {'upload_income_form': upload_income})
    
def update_income_categories(request, income_category_id):
    income_category_id = int(income_category_id)
    try:
        income_shelf = IncomeCategories.objects.get(id = income_category_id)
    except IncomeCategories.DoesNotExist:
        return redirect('settings_main')
    income_category_form = IncomeCategoriesCreate(request.POST or None, instance = income_shelf)
    if income_category_form.is_valid():
        income_category_form.save()
        return redirect('settings_main')
    return render(request, 'CRUD/upload_income_form.html', {'upload_income_form': income_category_form})

def delete_income_categories(request, income_category_id):
    income_category_id = int(income_category_id)
    try:
        income_category_form = IncomeCategories.objects.get(id = income_category_id)
    except IncomeCategories.DoesNotExist:
        return redirect('settings_main')
    income_category_form.delete()
    return redirect('settings_main')

#CRUD for income categories
def upload_expence(request):
    upload_expence = ExpenceCategoriesCreate()
    if request.method == 'POST':
        upload_expence = ExpenceCategoriesCreate(request.POST, request.FILES)
        if upload_expence.is_valid():
            upload_expence.save()
            return redirect('settings_main')
        else:
            return HttpResponse(""" Something went wrong. Please reload
              the webpage by clicking <a href = "{{url = 'settings_main'}}>Reload</a>"
              """)
    else:
        return render(request, 'CRUD/upload_expence_form.html', {'upload_expence_form': upload_expence})
    
def update_expence_categories(request, expence_category_id):
    expence_category_id = int(expence_category_id)
    try:
        expence_shelf = ExpenceCategories.objects.get(id = expence_category_id)
    except ExpenceCategories.DoesNotExist:
        return redirect('settings_main')
    expence_category_form = ExpenceCategoriesCreate(request.POST or None, instance = expence_shelf)
    if expence_category_form.is_valid():
        expence_category_form.save()
        return redirect('settings_main')
    return render(request, 'CRUD/upload_expence_form.html', {'upload_expence_form': expence_category_form})

def delete_expence_categories(request, expence_category_id):
    expence_category_id = int(expence_category_id)
    try:
        expence_category_form = ExpenceCategories.objects.get(id = expence_category_id)
    except ExpenceCategories.DoesNotExist:
        return redirect('settings_main')
    expence_category_form.delete()
    return redirect('settings_main')