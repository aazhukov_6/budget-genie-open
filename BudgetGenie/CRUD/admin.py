from django.contrib import admin
from .models import IncomeCategories

# Register your models here.
admin.site.register(IncomeCategories)