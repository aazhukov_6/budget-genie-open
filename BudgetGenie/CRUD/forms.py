from django import forms
from .models import IncomeCategories, ExpenceCategories

class IncomeCategoriesCreate(forms.ModelForm):

    class Meta:
        model = IncomeCategories
        fields='__all__'
class ExpenceCategoriesCreate(forms.ModelForm):

    class Meta:
        model = ExpenceCategories
        fields='__all__'