from django.db import models

# Create your models here.
class IncomeCategories(models.Model):
    income_category = models.CharField(unique = True, max_length=50)

    def __str__(self) -> str:
        return super().__str__()
    
class ExpenceCategories(models.Model):
    expence_category = models.CharField(unique = True, max_length=50)

    def __str__(self) -> str:
        return super().__str__()