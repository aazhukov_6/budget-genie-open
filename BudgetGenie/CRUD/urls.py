from django.urls import path
from . import views
from BudgetGenie.settings import DEBUG, STATIC_URL, STATIC_ROOT, MEDIA_ROOT, MEDIA_URL
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name = 'index'),
    path('settings/', views.settings, name = 'settings_main'),
    path('upload_income/', views.upload_income, name = 'upload-income-categories'),
    path('settings/update_income/<int:income_category_id>', views.update_income_categories),
    path('settings/delete_income/<int:income_category_id>', views.delete_income_categories),
    path('upload_expence/', views.upload_expence, name = 'upload-expence-categories'),
    path('settings/update_expence/<int:expence_category_id>', views.update_expence_categories),
    path('settings/delete_expence/<int:expence_category_id>', views.delete_expence_categories)
]

if DEBUG:
    urlpatterns += static(STATIC_URL, document_root = STATIC_ROOT)
    urlpatterns += static(MEDIA_URL, document_root = MEDIA_ROOT)